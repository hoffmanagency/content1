<?php

namespace craft\contentmigrations;

use Craft;
use craft\db\Migration;

/**
 * m180528_231348_content1_100 migration.
 */
class m180528_231348_content1_100 extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
      // Retreive your current Matrix field that you want to transmute
      $componentsList = Craft::$app->fields->getFieldByHandle("componentsList");

      // Create the new desired model to be added to the selected Matrix field
      $block = new \craft\models\MatrixBlockType([
      "fieldId" => $componentsList->id,
      "name" => "Vinc123",
      "handle" => "vinc123",
      // Fields belonging to the new block type
      "fields" => [
        new \craft\fields\PlainText([
          "name" => "Heading",
          "handle" => "heading",
          "instructions" => "A heading to go above another block type...",
          "required" => false,
          "placeholder" => "Type here...",
          "charLimit" => "",
          "multiline" => "",
          "initialRows" => "4",
          "columnType" => "string"
        ])
        // You can chain more here ...new \craft\fields\PlainText([])...
      ]
    ]);

      // Update the new transmuted Matrix field
      Craft::$app->matrix->saveBlockType($block);
      
      $componentsList = Craft::$app->getFields()->getFieldByHandle('componentsList');
      Craft::$app->getFields()->saveField($componentsList);

      return true;
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180528_231348_content1_100 cannot be reverted.\n";
        return false;
    }
}
